package com.example.avaliacaopratica

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


data class CarState(
    var model: String = "",
    var type: String = "",
    var price: Float  = 0.0F,
)