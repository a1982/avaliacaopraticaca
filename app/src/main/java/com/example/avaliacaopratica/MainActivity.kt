package com.example.avaliacaopratica


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.MutableLiveData

private val _addCarState = MutableLiveData(CarState())

class MainActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Forms()
        }
    }
}


@Composable
private fun Forms() {
    val arrayCars = remember{ mutableStateListOf<CarState>() }
    val modelState = remember {mutableStateOf("") }
    val priceState = remember {mutableStateOf("") }
    val selectedTypes = remember {
        mutableStateOf("")
    }
    val types = stringArrayResource(id = R.array.types)
    val isTypeOpen = remember {
        mutableStateOf(false)
    }

    Surface( elevation = 10.dp, shape = RoundedCornerShape(20.dp)){

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 60.dp)
        ) {

            OutlinedTextField(
                value = modelState.value,
                onValueChange = {
                    modelState.value = it
                    _addCarState.value = _addCarState.value?.copy(model = it)
                },
                label = { Text(text = stringResource(id = R.string.model)) }
            )


            //-------------------------------------------------------------------------------------------------
            //Icone de setinha
            val iconArrow = if (isTypeOpen.value) {
                Icons.Filled.KeyboardArrowUp
            } else {
                Icons.Filled.KeyboardArrowDown
            }
            //-------------------------------------------------------------------------------------------------
            Box {

                OutlinedTextField(
                    value = selectedTypes.value,
                    onValueChange = { selectedTypes.value = it },
                    readOnly = true,
                    label = { Text(text = stringResource(id = R.string.selectType)) },
                    trailingIcon = {
                        Icon(
                            iconArrow,
                            "",
                            Modifier.clickable { isTypeOpen.value = !isTypeOpen.value })
                    }
                )


                DropdownMenu(
                    expanded = isTypeOpen.value,
                    onDismissRequest = { isTypeOpen.value = false },
                    modifier = Modifier.width(280.dp)
                ) {
                    for (type in types) {
                        DropdownMenuItem(onClick = {
                            isTypeOpen.value = false
                            selectedTypes.value = type
                            _addCarState.value = _addCarState.value?.copy(type = type)
                        }) {
                            Text(text = type)
                        }
                    }
                }
            }

            OutlinedTextField(
                value = priceState.value,
                onValueChange = {
                    priceState.value = it
                },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                label = { Text(text = stringResource(id = R.string.price)) }
            )

            Button(onClick = {
                arrayCars.add(
                    CarState(
                        modelState.value,
                        selectedTypes.value,
                        priceState.value.toFloat()
                    )
                )
            }) {
                Text(text = stringResource(id = R.string.submit_button))
            }

            LazyColumn {
                items(arrayCars) {
                    CarListItem(it)
                }
            }
        }
    }
}


@Composable
fun CarListItem(car: CarState){

    val strikeThrough = remember {
        mutableStateOf(false)
    }
    val expanded = remember {
        mutableStateOf(false)
    }
    val extraPadding by animateDpAsState(
        if (expanded.value) 24.dp else 0.dp,
        animationSpec = spring(
            dampingRatio = Spring.DampingRatioMediumBouncy,
            stiffness = Spring.StiffnessLow
        )
    )

    Surface(shape = RoundedCornerShape(10.dp),
        modifier = Modifier
            .pointerInput(Unit) {
                detectTapGestures(onLongPress = { strikeThrough.value = !strikeThrough.value },
                    onTap = { expanded.value = !expanded.value }
                )
            }
            .padding(vertical = 4.dp, horizontal = 8.dp)){

        Column {

            Row(modifier = Modifier
                .height(60.dp)
                .fillMaxWidth()) {

                if (strikeThrough.value){
                    Column(modifier = Modifier
                        .fillMaxHeight()
                        .width(10.dp)
                        .background(Color.Red)) {
                        Text(text = "")
                    }
                }else{
                    Column(modifier = Modifier
                        .fillMaxHeight()
                        .width(10.dp)
                        .background(Color.Green)) {
                        Text(text = "")
                    }
                }


                Column(modifier = Modifier
                    .width(300.dp)
                    .fillMaxHeight()
                    .padding(horizontal = 10.dp),
                    verticalArrangement = Arrangement.Center) {
                    if (strikeThrough.value){
                        Text(text = car.model, style = TextStyle(textDecoration = TextDecoration.LineThrough))
                    }else{
                        Text(text = car.model)
                    }
                }

                Column(modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
                    .padding(end = 10.dp),
                    verticalArrangement = Arrangement.Center) {
                    if (strikeThrough.value){
                        Text(text = "Sold", fontStyle = FontStyle.Italic, modifier = Modifier.align(Alignment.End))
                    }else{
                        Text(text = "")
                    }
                }
            }

            if (expanded.value){
                Row(modifier = Modifier.fillMaxWidth()) {
                    Column(
                        modifier = Modifier.padding(
                            top = 5.dp,
                            start = 15.dp
                        )
                    ) {
                        Text(text = "Type: ", fontStyle = FontStyle.Italic)
                        Text(text = "Price: ", fontStyle = FontStyle.Italic)
                        Text(text = "Status: ", fontStyle = FontStyle.Italic)
                    }
                    Column(
                        modifier = Modifier.padding(
                            top = 5.dp,
                            start = 15.dp
                        )
                    ) {
                        Text(text = car.type)
                        Text(text = car.price.toString())
                        if (strikeThrough.value) {
                            Text(text = "This Vehicle is sold")
                        } else {
                            Text(text = "This Vehicle is available")
                        }
                    }
                }
            }
        }
    }
}



@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    //CarsList()
}